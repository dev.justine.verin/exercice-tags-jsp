<%@page import="fr.jv.listecompetences.CompetencesBean" %>
<%@ page import="java.util.Locale" %>
<!DOCTYPE html>

<html lang="de">
<head>
    <title><fmt:message key="pageTitle"/></title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

<h1><fmt:message key="formTitle"/></h1>
<br/>
<form action="controller">
    <h2><fmt:message key="instruction"/> </h2>
    <table>
        <caption> <fmt:message key="caption"/></caption>
        <thead>
            <tr>
                <th colspan="2"><fmt:message key="listTitle"/></th>
            </tr>
        </thead>
        <tbody>
        <tr>
            <td><label for="nom"><fmt:message key="name"/></label></td>
            <td><input type="text" id="nom" name="nom"></td>
        </tr>
        <c:forEach items="${CompetencesBean.LISTE_COMPETENCES}" var="competence">
            <tr>
                <td> <input type="checkbox" value="${competence}" name="competenceJava" id="${competence}"> </td>
                <td><label for="${competence}"><fmt:message key="${competence}"/></label></td>
            </tr>

        </c:forEach>



        <!--<mt:checkboxes competences="${CompetencesBean.LISTE_COMPETENCES}" inputName="competenceJava"/> --!>
        </tbody>
    </table>
    <input type="submit" value="<fmt:message key="submit"/>">
</form>

</body>
</html>