<%--
  Created by IntelliJ IDEA.
  User: justi
  Date: 24/01/2024
  Time: 13:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="mt" uri="/WEB-INF/mytags.tld" %>

<!DOCTYPE>
<html lang="fr">
<head>
    <title><fmt:message key="resultTitle"/></title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<jsp:useBean id="compBean" class="fr.jv.listecompetences.CompetencesBean" scope="request">

</jsp:useBean>
<h1><fmt:message key="resultTitle"/></h1>
<h2> <fmt:message key="thanks"><fmt:param value="${compBean.nom}"/></fmt:message></h2>
<table>
    <caption> <fmt:message key="resultsCaption"/> </caption>
    <c:choose>
        <c:when test="${compBean.competencesAcquises.size() == 0}">
            <tr>
                <th colspan="2"> <fmt:message key="noSkillsMessage"/></th>
            </tr>
        </c:when>
        <c:otherwise>
            <tbody class="acquis">
            <tr>
                <th colspan="2"><fmt:message key="acquiredSkills"/></th>
            </tr>
            <c:forEach items="${compBean.competencesAcquises}" var="competence">
                <tr>
                    <td>
                        <fmt:message key="${competence}"/>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </c:otherwise>
    </c:choose>
    <c:choose>
        <c:when test="${compBean.competencesNonAcquises.size() == 0}">
            <th colspan="2"><fmt:message key="allSkillsMessage"/></th>
        </c:when>
        <c:otherwise>
            <tbody class="non-acquis">
            <tr>
                <th colspan="2"><fmt:message key="notAcquiredSkills"/>:</th>
            </tr>
            <c:forEach items="${compBean.competencesNonAcquises}" var="competence">
                <tr>
                    <td>
                        <fmt:message key="${competence}"/>
                    </td>
                </tr>
            </c:forEach>

            </tbody>
        </c:otherwise>
    </c:choose>
</table>
</body>
</html>