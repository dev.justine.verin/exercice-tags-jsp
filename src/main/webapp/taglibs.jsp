<%@ page import="java.util.Locale" %>
<%@ taglib prefix="mt" uri="/WEB-INF/mytags.tld" %>
<%@ taglib prefix="fmt" uri="jakarta.tags.fmt"%>
<%@ taglib prefix="c" uri="jakarta.tags.core" %>
<fmt:setLocale value="en"/>
<fmt:setBundle basename="messages"/>