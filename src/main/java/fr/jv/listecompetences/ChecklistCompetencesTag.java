package fr.jv.listecompetences;

import jakarta.servlet.jsp.JspWriter;
import jakarta.servlet.jsp.tagext.SimpleTagSupport;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class ChecklistCompetencesTag extends SimpleTagSupport {
    private List<String> competences;
    private Locale currentLocale;
    private String inputName;
    @Override
    public void doTag() throws IOException {
        JspWriter out = getJspContext().getOut();
        for(String competence : competences){
            out.println("<tr>");
            out.println(String.format("<td><input type=checkbox name=%s value='<fmt:message key=%s/>'</td>",
                    inputName,
                    competence));
            out.println("<td>" + competence + "</td>");
            out.println("</tr>");

        }

    }

    public List<String> getCompetences() {
        return competences;
    }

    public void setCompetences(List<String> competences) {
        this.competences = competences;
    }

    public String getInputName() {
        return inputName;
    }

    public void setInputName(String inputName) {
        this.inputName = inputName;
    }


    public Locale getCurrentLocale() {
        return currentLocale;
    }

    public void setCurrentLocale(Locale currentLocale) {
        this.currentLocale = currentLocale;
    }
}