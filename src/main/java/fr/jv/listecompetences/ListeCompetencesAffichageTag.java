package fr.jv.listecompetences;


import jakarta.servlet.jsp.JspWriter;
import jakarta.servlet.jsp.tagext.SimpleTagSupport;

import java.io.IOException;
import java.util.ArrayList;

public class ListeCompetencesAffichageTag extends SimpleTagSupport {
    private ArrayList<String> competences;

    @Override
    public void doTag() throws IOException {
        JspWriter out = getJspContext().getOut();
        for(String competence : competences){
            out.println("<tr><td>");
            out.println(String.format("<fmt:message key='%s'/>", competence));
            out.println("</td></tr>");

        }
    }

    public ArrayList<String> getCompetences() {
        return competences;
    }

    public void setCompetences(ArrayList<String> competences) {
        this.competences = competences;
    }


}