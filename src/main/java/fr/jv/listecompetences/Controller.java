package fr.jv.listecompetences;

import java.io.*;
import java.util.Locale;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

@WebServlet(name = "controller", value = "/controller")
public class Controller extends HttpServlet {
    @Override
    public void init() {

    }
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        process(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
        process(request, response);

    }

    private void process(HttpServletRequest request, HttpServletResponse response) {
        String[] listeCompetencesAcquises = request.getParameterValues("competenceJava");
        String nom = request.getParameter("nom");
        CompetencesBean compBean = new CompetencesBean();
        compBean.setCompetencesAcquises(listeCompetencesAcquises);
        compBean.setNom(nom);
        request.setAttribute("compBean", compBean);
        try{
            request.getRequestDispatcher("bilan.jsp").forward(request, response);
        }catch(IOException | ServletException e){
            e.printStackTrace();
        }

    }
    @Override
    public void destroy() {
    }
}