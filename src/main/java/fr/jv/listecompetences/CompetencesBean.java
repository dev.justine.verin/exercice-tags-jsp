package fr.jv.listecompetences;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CompetencesBean {

    public static final List<String> LISTE_COMPETENCES = new ArrayList<>(List.of("classes", "types",
            "exceptions",
            "javafx", "jakartaee"));


    /*public static final List<String> LISTE_COMPETENCES_DE = new ArrayList<>(List.of("Klassen", "Primitiven " +
            "Datentypen", "Exceptions", "JavaFX", "Jakarta EE"));

   public static final List<String> LISTE_COMPETENCES_EN = new ArrayList<>(List.of("Classes", "Primitive types",
            "Exceptions", "JavaFX", "Jakarta EE" ));*/

    private ArrayList<String> competencesAcquises;

    private ArrayList<String> competencesNonAcquises;

    private String nom;

    public CompetencesBean(){
        competencesAcquises = new ArrayList<>();
        competencesNonAcquises = new ArrayList<>();

    }

    public ArrayList<String> getCompetencesAcquises() {
        return competencesAcquises;
    }

    public void setCompetencesAcquises(String[] competencesSaisies) {
        if(competencesSaisies != null){
            this.competencesAcquises.addAll(Arrays.asList(competencesSaisies));

        }
        deduireCompetencesNonAcquises();
    }



    private void deduireCompetencesNonAcquises() {
        for(String competence : LISTE_COMPETENCES){
            if(!competencesAcquises.contains(competence)){
                competencesNonAcquises.add(competence);
            }
        }
    }

    public ArrayList<String> getCompetencesNonAcquises() {
        return competencesNonAcquises;
    }

    public void setCompetencesNonAcquises(ArrayList<String> competencesNonAcquises) {
        this.competencesNonAcquises = competencesNonAcquises;
    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }





}